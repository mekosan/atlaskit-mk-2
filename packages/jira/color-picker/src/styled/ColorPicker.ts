import styled from 'styled-components';
import { gridSize } from '@atlaskit/theme';

export const ColorCardWrapper = styled.div`
  display: inline-block;
  padding: 0 ${gridSize() + 2}px;
`;
